import Vue from 'vue'

export async function fetchUsers({commit}){
    commit('setLoading', true,{root :true})
    let {data} = await Vue.axios({
        method : "get",
        url : '/users'
    })
    commit('setLoading', false,{root :true})
    if(data.success){
        commit('setUsers',data);
    }
}

export async function addUser({commit},user) {
    commit('setLoading', true,{root :true});
    let {data} = await Vue.axios({
        method : "post",
        url : "users",
        data : user
    });
    commit('setLoading', false,{root :true});
    if(data.success){
        commit('pushUser',data);
    }
    return data;
}
export async function destroyUser({commit},id){
    let {data} = await Vue.axios({
        method : 'delete',
        url : `users/${id}`,
    });
    if(data.success){
        commit('removeUser',id);
    }
}

export async function uploadAvatar({commit},{formData,id}){
    let {data} = await Vue.axios({
        method : "post",
        url : `upload/${id}`,
        data : formData
    });
    if(data.success){
        commit('pushUser',data);
    }
    return data;
}
