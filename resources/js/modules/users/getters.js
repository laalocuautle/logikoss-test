export function users(state) {
    return state.users;
}

export function user(state) {
    return state.user;
}
