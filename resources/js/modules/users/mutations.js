export function setUsers(state,{data}) {
    state.users = data;
}
export function pushUser(state,{user}) {
    state.users.push(user);
}

export function removeUser(state,id) {
    let users = state.users;
    users =  users.filter(u => u.id !== id);
    state.users = users;
}
