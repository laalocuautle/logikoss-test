import Vue from 'vue'
import Vuex from 'vuex'
import users from '@/js/modules/users';
Vue.use(Vuex);
export default new Vuex.Store({
    state : {
        loading : false
    },
    modules : {
        users
    },
    mutations :{
        setLoading(state,bool){
            state.loading = bool;
        }
    }
})
