@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                <button class="btn btn-info text-white" type="button">
                    <a class="text-white" href="{{route('posts.create')}}">
                        Crear Post
                    </a>
                </button>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 col-md-12 col-sm-12 text-right">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Titulo</th>
                        <th scope="col">Contenido</th>
                        <th scope="col">Imagen</th>
                        <th scope="col">Slug</th>
                        <th scope="col">
                            Acciones
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <th scope="row">
                                {{$post->id}}
                            </th>
                            <td>{{$post->title}}</td>
                            <td>{{$post->content}}</td>
                            <td>
                                @if($post->image)
                                    <img src="{{$post->image}}" alt="{{$post->title}}" class="img-thumbnail">
                                @endif
                            </td>
                            <td>{{$post->slug}}</td>
                            <td>
                                <a href="{{route('posts.destroy',['post' => $post->id])}}">
                                    <button class="btn btn-danger" type="button">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 col-md-12 col-sm-12 text-center">
                {{$posts->links()}}
            </div>
        </div>
    </div>
@endsection
